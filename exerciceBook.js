'use strict';

// Quelques livres pour l'exemple...
const books = [
  {
    "title": "Unlocking Android",
    "isbn": "1933988673",
    "pageCount": 416,
    "publishedDate": { "$date": "2009-04-01T00:00:00.000-0700" },
    "thumbnailUrl": "https://s3.amazonaws.com/AKIAJC5RLADLUMVRPFDQ.book-thumb-images/ableson.jpg",
    "shortDescription": "Unlocking Android: A Developer's Guide provides concise, hands-on instruction for the Android operating system and development tools. This book teaches important architectural concepts in a straightforward writing style and builds on this with practical and useful examples throughout.",
    "status": "PUBLISH",
    "authors": ["W. Frank Ableson", "Charlie Collins", "Robi Sen"],
    "categories": ["Open Source", "Mobile"]
  },
  {
    "title": "Android in Action, Second Edition",
    "isbn": "1935182722",
    "pageCount": 592,
    "publishedDate": { "$date": "2011-01-14T00:00:00.000-0800" },
    "thumbnailUrl": "https://s3.amazonaws.com/AKIAJC5RLADLUMVRPFDQ.book-thumb-images/ableson2.jpg",
    "shortDescription": "Android in Action, Second Edition is a comprehensive tutorial for Android developers. Taking you far beyond \"Hello Android,\" this fast-paced book puts you in the driver's seat as you learn important architectural concepts and implementation strategies. You'll master the SDK, build WebKit apps using HTML 5, and even learn to extend or replace Android's built-in features by building useful and intriguing examples. ",
    "status": "PUBLISH",
    "authors": ["W. Frank Ableson", "Robi Sen"],
    "categories": ["Java"]
  },
  {
    "title": "Specification by Example",
    "isbn": "1617290084",
    "pageCount": 0,
    "publishedDate": { "$date": "2011-06-03T00:00:00.000-0700" },
    "thumbnailUrl": "https://s3.amazonaws.com/AKIAJC5RLADLUMVRPFDQ.book-thumb-images/adzic.jpg",
    "status": "PUBLISH",
    "authors": ["Gojko Adzic"],
    "categories": ["Software Engineering"]
  },
  {
    "title": "Flex 3 in Action",
    "isbn": "1933988746",
    "pageCount": 576,
    "publishedDate": { "$date": "2009-02-02T00:00:00.000-0800" },
    "thumbnailUrl": "https://s3.amazonaws.com/AKIAJC5RLADLUMVRPFDQ.book-thumb-images/ahmed.jpg",
    "longDescription": "New web applications require engaging user-friendly interfaces   and the cooler, the better. With Flex 3, web developers at any skill level can create high-quality, effective, and interactive Rich Internet Applications (RIAs) quickly and easily. Flex removes the complexity barrier from RIA development by offering sophisticated tools and a straightforward programming language so you can focus on what you want to do instead of how to do it. And now that the major components of Flex are free and open-source, the cost barrier is gone, as well!    Flex 3 in Action is an easy-to-follow, hands-on Flex tutorial. Chock-full of examples, this book goes beyond feature coverage and helps you put Flex to work in real day-to-day tasks. You'll quickly master the Flex API and learn to apply the techniques that make your Flex applications stand out from the crowd.    Interesting themes, styles, and skins  It's in there.  Working with databases  You got it.  Interactive forms and validation  You bet.  Charting techniques to help you visualize data  Bam!  The expert authors of Flex 3 in Action have one goal   to help you get down to business with Flex 3. Fast.    Many Flex books are overwhelming to new users   focusing on the complexities of the language and the super-specialized subjects in the Flex eco-system; Flex 3 in Action filters out the noise and dives into the core topics you need every day. Using numerous easy-to-understand examples, Flex 3 in Action gives you a strong foundation that you can build on as the complexity of your projects increases.",
    "status": "PUBLISH",
    "authors": ["Tariq Ahmed with Jon Hirschi", "Faisal Abid"],
    "categories": ["Internet"]
  }
]


/** Ajouter la cle dans un Element dt, la valeur dans un Element,
 * les deux attaches comme fils à parentElt.
 */
function toDtDdDom(cle, valeur, parentElt) {
    const dt = document.createElement('dt');
    dt.innerText = '' + cle;
    parentElt.appendChild(dt);
    dt.classList.add(cle)

    const dl = document.createElement('dd');
    dl.innerText = '' + valeur;
    parentElt.appendChild(dl);
}

/** Transformer un objet en chaine de caracteres.
 * Si l'objet est un tableau, la chaine correspond à la chaine representant ses elements, joints par ", ".
 */
function myToString(obj) {
    if (Array.isArray(obj)) {
        console.log("myToString:", obj);
        return obj.map( x => myToString(x) ).join(', ');
    } else {
        return '' + obj;
    }
}

/** Construire un element dl contenant les proprietes de l'objet.
 * Le nom d'une propriete apparait en dt et sa valeur en dd.
 */
function propertiesToDL(objet) {
    const dl = document.createElement('dl');
    Object.entries(objet).forEach(
        ([name, value]) => {
            console.log(name, '==>', value);
            toDtDdDom(name, myToString(value), dl);
        }
    );
    return dl;
}

/** Construire un fragment de document DOM pour representer un livre.
 * Quelle est le contenu de ce fragment ?
 */
function bookToDOM(book) {
    console.log(book);
    const fragment = document.createDocumentFragment();
    const div = document.createElement('div');
    div.appendChild(document.createTextNode(book.title));
    div.classList.add('title');
    fragment.appendChild(div)
    const dl = propertiesToDL(book);
    fragment.appendChild(dl);
    return fragment;
}



/** Produire un DOM pour représenter un tableau de livres,
 * ajoutés comme dernier de l'élément body.
 */
function display(books) {
    // Construire les nouveaux éléments
    const ol = document.createElement('ol');
    // ol.attributes.setNamedItem(document.createAttribute("type", "1"));
    // ol.attributes.setNamedItem(document.createAttribute("start", "1")); // dÃ©prÃ©ciÃ©
    books
        .map( book => bookToDOM(book) )
        .forEach((bookDom, indice) =>{
          const li = document.createElement('li')
          li.id = books[indice].isbn
          ol.appendChild(li).appendChild(bookDom) 
        });
        
        

    // Les accrocher à l'élément body du document
    const body = document.getElementsByTagName('body')[0];
    body.appendChild(ol);
}


/** Récupérer une liste de livres depuis le document précédents.
  * Les livres n'auront que les champs : title, pageCount, categories et authors.
  * @return un tableau de livre, chaque livre a les attributs ci-dessus.
  */
function domBooksToJSON() {
  
    const all_li= document.getElementsByTagName('li')
    return [...all_li].map(one_li => {return
      {
        title: one_li.querySelector('dt.title') .nextElementSibling.innerText;
        pageCount: one_li.querySelector('dt.pageCount') . nextElementSibling.innerText;  
        categories: one_li.querySelector('dt.categories') .nextElementSibling.innerText;
        authors: one_li.querySelector('dt.authors') .nextElementSibling.innerText                        
    }})
     

}   
console.log(domBooksToJSON())


/** Produit un élément table qui contient les informations sur books,
 * un tableau de books avec les attributs précisés.
 */
function booksToTable(books, attributs = ['title', 'pageCount', 'categories', 'authors']) {
   //TODO : à faire
   const numCol= attributs.length; //Création du nb de colonnes suivant la taille du tab "attributs".
   const numLigne= books.length; //Création du nb de lignes suivant la tailles des objets livres.
   const tableElem= document.createElement('table'); //Création de l'élement table.
   
   //Initialisation de la ligne d'en-tête où seront affichés les "attributs". 
   var rowAttr = document.createElement('tr');
   for (var i = 0; i < numCol; i++) {
      var entete= document.createElement('td')
      entete.innerText= attributs[i]
      rowAttr.appendChild(entete)
   }
   tableElem.appendChild(rowAttr);

   //Initialisation de lignes correspondant aux nombres de livres dans l'objet "books". 
   for (var j= 0; j < numLigne; j++){
      const ligneBook= document.createElement('tr');
      for (var k= 0; k < numCol; k++){
         const colBook= document.createElement('td');
         colBook.innerHTML = books[j][attributs[k]]; 
         ligneBook.appendChild(colBook);

      }
      tableElem.appendChild(ligneBook);
   }
   const nodeChild= document.body.firstElementChild
   document.body.insertBefore(tableElem,nodeChild); //Place la table en haut de la page.
}

booksToTable(books);

document.addEventListener(window.load, display(books));




let btnCache = document.getElementById("cacher");
let btnMontre = document.getElementById("montrer");
 
btnCache.addEventListener('click', event =>  {
   const tableBook = document.getElementsByTagName('li');
      [...tableBook].forEach(li => {
      if (li.querySelector("dt.shortDescription").nextElementSibling.classList.contains("cache") === false) {
         li.querySelector("dt.shortDescription").nextElementSibling.classList.add("cache");
         }
      })

})

btnMontre.addEventListener('click', event =>  {
   const tableBook = document.getElementsByTagName('li');
   [...tableBook].forEach(li => {
    if (li.querySelector("dt.shortDescription").nextElementSibling.classList.contains("cache")) {
      li.querySelector("dt.shortDescription").nextElementSibling.classList.remove("cache");
      }
   })

})




let btnTitre = document.getElementsByClassName('title');
   [...btnTitre].forEach(btn => {
    btn.addEventListener('click', btnClick => {
      btnClick.target.nextElementSibling.classList.toggle("cache");     
    })
    btn.addEventListener('mouseover', event => (document.body.style.cursor = "pointer"))      
    btn.addEventListener('mouseout', event => (document.body.style.cursor = "auto"))    
   })

